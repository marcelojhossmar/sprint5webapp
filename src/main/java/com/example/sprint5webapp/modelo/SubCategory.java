package com.example.sprint5webapp.modelo;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class SubCategory extends ModelBase {

    private String name;
    private String code;
    @OneToOne(optional = false)
    private Category category;


    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public Category getCategory() {
        return category;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
