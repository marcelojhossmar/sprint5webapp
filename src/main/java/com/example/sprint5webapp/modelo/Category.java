package com.example.sprint5webapp.modelo;

import javax.persistence.Entity;

@Entity
public class Category extends ModelBase {
    private String name;

    public String getCode() {
        return code;
    }

    private String code;

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

