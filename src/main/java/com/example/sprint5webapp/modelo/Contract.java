package com.example.sprint5webapp.modelo;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class Contract extends ModelBase {
    @OneToOne(optional = false)
    private Employee employee;
    @OneToOne(optional = false)
    private Position position;
    private Date initData;
    private Date endDate;

    public Employee getEmployee() {
        return employee;
    }

    public Position getPosition() {
        return position;
    }

    public Date getInitData() {
        return initData;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setInitData(Date initData) {
        this.initData = initData;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
