package com.example.sprint5webapp.modelo;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Item extends ModelBase{
    private String name;
    private String code;
    @OneToOne(optional = false)
    private SubCategory subCategory;

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }
}
