package com.example.sprint5webapp.modelo;
import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;


@Entity

public class Employee extends ModelBase
{   private String firstName, lastName;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "employee",cascade = CascadeType.REMOVE)
    private  List<Contract> contracts = new LinkedList<Contract>();

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public List<Contract> getContracts() {
        return contracts;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }
}
