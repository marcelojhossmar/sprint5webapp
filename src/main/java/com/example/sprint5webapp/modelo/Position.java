package com.example.sprint5webapp.modelo;

import javax.persistence.Entity;

@Entity
public class Position extends ModelBase {
    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
