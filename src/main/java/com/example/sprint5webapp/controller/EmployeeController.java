package com.example.sprint5webapp.controller;

import com.example.sprint5webapp.repositories.EmployeeRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class EmployeeController {
    private EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    /**
     *
     * Este metodo se encarga de buscar a todos los empleados registrados
     */
    @RequestMapping("/employees")
    public String getEmployee (Model model){
        model.addAttribute("employee", employeeRepository.findAll()); // recupera de la DB por debajo. query
        return "employees";




    }
}
