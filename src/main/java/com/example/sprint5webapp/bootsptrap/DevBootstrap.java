package com.example.sprint5webapp.bootsptrap;



import com.example.sprint5webapp.modelo.*;
import com.example.sprint5webapp.repositories.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private CategoryRepository categoryRepository;
    private SubCategoryRepository subCategoryRepository;
    private ItemRepository itemRepository;
    private EmployeeRepository employeeRepository;
    private PositionRepository positionRepository;
    private ContractRepository contractRepository;

    public DevBootstrap(CategoryRepository categoryRepository, SubCategoryRepository subCategoryRepository, ItemRepository itemRepository, EmployeeRepository employeeRepository, PositionRepository positionRepository, ContractRepository contractRepository ){
        this.categoryRepository = categoryRepository;
        this.subCategoryRepository = subCategoryRepository;
        this.itemRepository = itemRepository;
        this.employeeRepository = employeeRepository;
        this.positionRepository = positionRepository;
        this.contractRepository = contractRepository;


    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        initData();
    }

    private void initData() {
        //  categorioa eppcategory

        Category eppCategory = new Category();
        eppCategory.setCode("EPP");
        eppCategory.setName("EPP-NAME");
        categoryRepository.save(eppCategory);

        //categoria resourceCAtegory
        Category resourceCategory = new Category();
        resourceCategory.setCode("RESOURCE");
        resourceCategory.setName("RESOURCE-NAME");
        categoryRepository.save(resourceCategory);

        //SubCAtegoria Safely
        SubCategory safelyCategory = new SubCategory();
        safelyCategory.setCategory(eppCategory);
        safelyCategory.setCode("SAF");
        safelyCategory.setName("SAFELY");
        subCategoryRepository.save(safelyCategory);

        // Subcategorio material
        SubCategory rawMaterialSubCAtegory = new SubCategory();
        rawMaterialSubCAtegory.setCategory(resourceCategory);
        rawMaterialSubCAtegory.setCode("RM");
        rawMaterialSubCAtegory.setName("RAWMATERIAL");
        subCategoryRepository.save(rawMaterialSubCAtegory);

        // COPIADO

        // Helmet Item
        Item helmet = new Item();
        helmet.setCode("HEL");
        helmet.setName("HELMET");
        helmet.setSubCategory(safelyCategory);

        itemRepository.save(helmet);

        // ink Item
        Item ink = new Item();
        ink.setCode("INK");
        ink.setName("INK");
        ink.setSubCategory(rawMaterialSubCAtegory);
        itemRepository.save(ink);

        // John Employee
        Employee john = new Employee();
        john.setFirstName("John");
        john.setLastName("Doe");

        // Position
        Position position = new Position();
        position.setNombre("OPERATIVE");
        positionRepository.save(position);

        // contract
        Contract contract = new Contract();
        contract.setEmployee(john);
        contract.setInitData(new Date(2010, 1, 1));
        contract.setPosition(position);

        john.getContracts().add(contract);
        employeeRepository.save(john);
        contractRepository.save(contract);


    }
}
