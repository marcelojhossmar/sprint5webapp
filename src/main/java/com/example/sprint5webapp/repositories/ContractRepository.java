package com.example.sprint5webapp.repositories;

import com.example.sprint5webapp.modelo.Contract;
import com.example.sprint5webapp.modelo.Employee;
import org.springframework.data.repository.CrudRepository;

public interface ContractRepository extends CrudRepository<Contract, Long> {
}
