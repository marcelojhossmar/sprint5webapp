package com.example.sprint5webapp.repositories;

import com.example.sprint5webapp.modelo.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Long>  {

}
