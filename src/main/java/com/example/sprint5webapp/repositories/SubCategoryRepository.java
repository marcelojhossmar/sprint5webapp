package com.example.sprint5webapp.repositories;

import com.example.sprint5webapp.modelo.Employee;
import com.example.sprint5webapp.modelo.SubCategory;
import org.springframework.data.repository.CrudRepository;

public interface SubCategoryRepository extends CrudRepository<SubCategory, Long> {
}
