package com.example.sprint5webapp.repositories;

import com.example.sprint5webapp.modelo.Employee;
import com.example.sprint5webapp.modelo.ModelBase;
import org.springframework.data.repository.CrudRepository;

public interface ModelBaseRepository extends CrudRepository<ModelBase, Long> {
}
