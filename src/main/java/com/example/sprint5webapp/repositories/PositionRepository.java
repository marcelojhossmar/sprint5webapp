package com.example.sprint5webapp.repositories;

import com.example.sprint5webapp.modelo.Employee;
import com.example.sprint5webapp.modelo.Position;
import org.springframework.data.repository.CrudRepository;

public interface PositionRepository extends CrudRepository<Position, Long> {
}
