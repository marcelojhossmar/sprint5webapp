package com.example.sprint5webapp.repositories;

import com.example.sprint5webapp.modelo.Category;
import com.example.sprint5webapp.modelo.Employee;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Long> {
}
