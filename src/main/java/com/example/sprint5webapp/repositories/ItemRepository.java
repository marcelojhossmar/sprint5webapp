package com.example.sprint5webapp.repositories;

import com.example.sprint5webapp.modelo.Employee;
import com.example.sprint5webapp.modelo.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item, Long> {
}
